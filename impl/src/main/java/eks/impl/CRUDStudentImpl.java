package eks.impl;

import eks.data.AlleStudenten;
import eks.data.Student;
import eks.service.CRUDStudent;

public class CRUDStudentImpl implements CRUDStudent {

    private AlleStudenten alleStudenten;

    public CRUDStudentImpl(){
        alleStudenten = AlleStudenten.getInstance();
    }

    @Override
    public Student readStudent(int mnr) {
        return alleStudenten.getStudent(mnr);
    }

    @Override
    public boolean createStudent(Student s) {
        return alleStudenten.createStudent(s);
    }
}
