module eks.impl {
    requires transitive eks.service;

    exports eks.impl;

    provides eks.service.CRUDStudent with eks.impl.CRUDStudentImpl;
}