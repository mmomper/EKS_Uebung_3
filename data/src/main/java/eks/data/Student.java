package eks.data;

public class Student {
    private int matrikelnr;
    private String name;


    public Student() {
    }

    public Student(int matrikelnr, String name) {
        this.matrikelnr = matrikelnr;
        this.name = name;
    }

    public int getMatrikelnr() {
        return matrikelnr;
    }

    public void setMatrikelnr(int matrikelnr) {
        this.matrikelnr = matrikelnr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "matrikelnr=" + matrikelnr +
                ", name='" + name + '\'' +
                '}';
    }
}
