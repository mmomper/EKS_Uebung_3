package eks.data;

import java.util.ArrayList;
import java.util.List;

public class AlleStudenten {
    private List<Student> alleStudenten;
    private static AlleStudenten instance = null;

    private AlleStudenten() {
        alleStudenten = new ArrayList<>();
    }

    public boolean createStudent(Student s) {
        alleStudenten.add(s);
        return true;
    }

    public Student getStudent(int mnr) {
        for (Student s : alleStudenten) {
            if (s.getMatrikelnr() == mnr) {
                return s;
            }
        }
        return null;
    }

    public static AlleStudenten getInstance() {
        if (instance == null) instance = new AlleStudenten();
        return instance;
    }
}
