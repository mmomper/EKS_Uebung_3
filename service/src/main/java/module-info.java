module eks.service {
    requires transitive eks.data;
    exports eks.service;
}