package eks.service;

import eks.data.Student;

public interface CRUDStudent {
    public Student readStudent(int mnr);
    public boolean createStudent(Student s);
}
