package eks.customer;

import eks.data.Student;
import eks.service.CRUDStudent;

import java.util.Iterator;
import java.util.ServiceLoader;

public class Customer {

    public static void main(String[] args) {
        ServiceLoader<CRUDStudent> loaderStudent = ServiceLoader.load(CRUDStudent.class);
        Iterator<CRUDStudent> it = loaderStudent.iterator();
        CRUDStudent crud = it.next();
        crud.createStudent(new Student(123456, "Test1"));
        crud.createStudent(new Student(234567, "Test2"));
        crud.createStudent(new Student(345678, "Test3"));
        System.out.println(crud.readStudent(123456));
        System.out.println(crud.readStudent(234567));
        System.out.println(crud.readStudent(345678));
    }
}
